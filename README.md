A repository to collect SuperBuild artifacts.

# Motivation

Creating a SuperBuild is time consuming and it does not evolve frequently.
This repository is used as a cache for pre-built SuperBuild XDK.

# Strategy


There is a single file in each commit: `SuperBuild_Install.tar`

This file is managed by Git-LFS in order to avoid problems with huge Git
repository.

There is many branches, following the convention platform/id where id is
computed from the content of SuperBuild directory.
It would be useful to use the SHA1 of the last commit changing the SuperBuild
path inside the OTB repository, but this information is not necessarily
available on the Continuous Integration process as the clone is a shallow clone.

The `master` branch should be kept without any `SuperBuild_Install.tar` file.
